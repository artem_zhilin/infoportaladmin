'use strict';

/**
 * News.js controller
 *
 * @description: A set of overridden actions for managing `News`.
 */

module.exports = {

  /**
   * Retrieve a news record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    const _id = ctx.params._id;
    if (!_id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }
    strapi.services.news.increaseViews(_id);
    return strapi.services.news.findOne(ctx.params);
  },

  /**
   * Increase commmentsCount on a news record
   * 
   * @return {Object}
   */
  increaseComments: async (ctx) => {
    return strapi.services.news.increaseComments(ctx.params._id);
  },
};
