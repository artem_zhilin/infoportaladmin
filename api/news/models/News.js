'use strict';
const _ = require('lodash');
const { transformToSlug } = require('../../utils');

/**
 * Lifecycle callbacks for the `News` model.
 */

module.exports = {
  // Before saving a value.
  // Fired before an `insert` or `update` query.
  // beforeSave: async (model) => {
  //   console.log(model);
  //   if (_.isEmpty(model.slug)) {
  //     model.slug = _.lowerCase(CyrillicToTranslit().transform(model.title, '-'));
  //   }
  // },

  // After saving a value.
  // Fired after an `insert` or `update` query.
  // afterSave: async (model, result) => {},

  // Before fetching all values.
  // Fired before a `fetchAll` operation.
  // beforeFetchAll: async (model) => {},

  // After fetching all values.
  // Fired after a `fetchAll` operation.
  // afterFetchAll: async (model, results) => {},

  // Fired before a `fetch` operation.
  // beforeFetch: async (model) => {},

  // After fetching a value.
  // Fired after a `fetch` operation.
  // afterFetch: async (model, result) => {},

  // Before creating a value.
  // Fired before an `insert` query.
  beforeCreate: async (model) => {
    if (_.isEmpty(model.slug)) {
      model.slug = transformToSlug(model.title);
    }
  },

  // After creating a value.
  // Fired after an `insert` query.
  // afterCreate: async (model, result) => {},

  // Before updating a value.
  // Fired before an `update` query.
  beforeUpdate: async (query) => {
    const updatedFields = query.getUpdate();
    if (updatedFields.title && _.isEmpty(updatedFields.slug)) {
      query.update({ 'slug':  transformToSlug(updatedFields.title) });
    }
  },

  // After updating a value.
  // Fired after an `update` query.
  // afterUpdate: async (model, result) => {},

  // Before destroying a value.
  // Fired before a `delete` query.
  // beforeDestroy: async (model) => {},

  // After destroying a value.
  // Fired after a `delete` query.
  // afterDestroy: async (model, result) => {}
};
