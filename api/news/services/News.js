'use strict';

/* global News */

/**
 * News.js service
 *
 * @description: A set of overridden functions similar to controller's actions to avoid code duplication.
 */

// Public dependencies.

module.exports = {
  

  increaseViews: async (_id) => {
    const news = await News.findById(_id);
    return News.updateOne({ _id }, { viewsCount: ++news.viewsCount });
  },

  increaseComments: async (_id) => {
    const news = await News.findById(_id);
    return News.updateOne({ _id }, { commentsCount: ++news.commentsCount });
  },
};
