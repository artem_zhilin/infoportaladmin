const CyrillicToTranslit = require('cyrillic-to-translit-js');


module.exports = {
  transformToSlug: input => CyrillicToTranslit().transform(input, '-').toLowerCase().replace(/\"/g, ''),
};