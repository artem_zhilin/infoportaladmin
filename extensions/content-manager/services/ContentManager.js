'use strict';

const _ = require('lodash');

const addFieldToValues = (modelName, values, field, value) => {
  if (strapi.models[modelName].attributes.hasOwnProperty(field)) {
    values.fields[field] = value;
  }
};

/**
 * A set of overridden actions for `ContentManager`
 */

module.exports = {

  add: async (params, values, source) => {
    // Multipart/form-data.
    if (values.hasOwnProperty('fields') && values.hasOwnProperty('files')) {
      // Silent recursive parser.
      const parser = value => {
        try {
          value = JSON.parse(value);
        } catch (e) {
          // Silent.
        }

        return _.isArray(value) ? value.map(obj => parser(obj)) : value;
      };

      for (let field of Object.getOwnPropertyNames(values.files)) {
        addFieldToValues(params.model, values, `has${_.upperFirst(field)}`, true);
      }

      const files = values.files;

      // Parse stringify JSON data.
      values = Object.keys(values.fields).reduce((acc, current) => {
        acc[current] = parser(values.fields[current]);

        return acc;
      }, {});

      // Update JSON fields.
      const entry = await strapi.plugins['content-manager'].queries(params.model, source).create({
        values,
      });

      // Then, request plugin upload.
      if (strapi.plugins.upload && Object.keys(files).length > 0) {
        // Upload new files and attach them to this entity.
        await strapi.plugins.upload.services.upload.uploadToEntity(
          {
            id: entry.id || entry._id,
            model: params.model,
          },
          files,
          source,
        );
      }

      return strapi.plugins['content-manager'].queries(params.model, source).findOne({
        id: entry.id || entry._id,
      });
    }
    
    // Create an entry using `queries` system
    return await strapi.plugins['content-manager'].queries(params.model, source).create({
      values,
    });
  },

  edit: async (params, values, source) => {
    // Multipart/form-data.
    if (values.hasOwnProperty('fields') && values.hasOwnProperty('files')) {
      // Silent recursive parser.
      const parser = value => {
        try {
          value = JSON.parse(value);
        } catch (e) {
          // Silent.
        }

        return _.isArray(value) ? value.map(obj => parser(obj)) : value;
      };

      const uploadFields = strapi.models[params.model].associations.filter(assoc => assoc.plugin === 'upload').map(assoc => assoc.alias);
      console.log(values.fields);
      for (let field of uploadFields) {
        addFieldToValues(params.model, values, `has${_.upperFirst(field)}`, false);
      }

      const files = values.files;

      // set empty attributes if old values was cleared
      _.difference(Object.keys(files), Object.keys(values.fields)).forEach(
        attr => {
          values.fields[attr] = [];
        },
      );

      for (let field of uploadFields) {
        if (values.fields[field] !== '[]' && values.fields[field] !== '{}' && values.fields[field] !== '') {
          addFieldToValues(params.model, values, `has${_.upperFirst(field)}`, true);                
        }
      }

      for (let field of Object.getOwnPropertyNames(values.files)) {
        addFieldToValues(params.model, values, `has${_.upperFirst(field)}`, true);    
      }

      // Parse stringify JSON data.
      values = Object.keys(values.fields).reduce((acc, current) => {
        acc[current] = parser(values.fields[current]);

        return acc;
      }, {});

      // Update JSON fields.
      await strapi.plugins['content-manager'].queries(params.model, source).update({
        id: params.id,
        values,
      });

      // Then, request plugin upload.
      if (strapi.plugins.upload) {
        // Upload new files and attach them to this entity.
        await strapi.plugins.upload.services.upload.uploadToEntity(
          params,
          files,
          source,
        );
      }

      return strapi.plugins['content-manager'].queries(params.model, source).findOne({
        id: params.id,
      });
    }

    // Raw JSON.
    return strapi.plugins['content-manager'].queries(params.model, source).update({
      id: params.id,
      values,
    });
  },
};
