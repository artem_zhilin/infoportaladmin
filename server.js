const strapi = require('strapi');

if (process.env.NODE_ENV !== 'test') {
  strapi(/* {...} */).start();
}

module.exports = strapi;