//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let { transformToSlug } = require('../api/utils');
let chai = require('chai');
let chaiHttp = require('chai-http');
let strapi = require('../server');
let expect = chai.expect;
chai.use(chaiHttp);

let models = {};
let serverUrl = 'http://localhost:1337';
let initialTags = [{name: 'tag1'}];

/*
* Test news crud operations
*/
describe('News crud operations', () => {
  before((done) => {
    strapi.start().then(async () => {
      models = strapi.models;
      serverUrl = strapi.config.url;
      await models.tag.deleteMany({});
      initialTags = await models.tag.create(initialTags);
      await models.news.deleteMany({});
      done();
    });
  });

  // after(() => {
  //   strapi.stop();
  // });

  it('it should GET all the News', (done) => {
    chai.request(serverUrl)
      .get('/news')
      .end((err, res) => {
        expect(res.statusCode).to.eql(200);
        expect(res.body).to.eql([]);
        done();
      });
  });

  it('it should CREATE news', (done) => {
    const news = {
      title: "Название новости авдадвадвадвдв",
      body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      tags: initialTags.map(tag => tag._id),
    };
    chai.request(serverUrl)
      .post('/news')
      .send(news)
      .end((err, res) => {
        expect(res.statusCode).to.eql(200);
        expect(res.body).to.be.an('object');
        expect(res.body).to.have.own.property('_id').that.is.not.empty;
        expect(res.body.slug).to.eql(transformToSlug(news.title));
        expect(res.body.tags).to.be.an('array').that.have.lengthOf(1)
          .and.have.nested.property('[0]._id', initialTags[0]._id.toString());
        done();
      });
  });

  it('it should not CREATE news without body', (done) => {
    const news = {
      title: "Название новости авдадвадвадвдв",
    };
    chai.request(serverUrl)
      .post('/news')
      .send(news)
      .end((err, res) => {
        expect(res.statusCode).to.eql(500);
        done();
      });
  });

  it('it should UPDATE news', (done) => {
    const news = {
      title: "Название новости авдадвадвадвдв",
      body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    };
    chai.request(serverUrl)
      .post('/news')
      .send(news)
      .end((err, res) => {
        const newsId = res.body._id;
        const newsBody = "dummy text dummy text dummy text dummy text dummy text dummy textdummy text Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop";
        chai.request(serverUrl)
          .put(`/news/${newsId}`)
          .send({ body: newsBody })
          .end((err, res) => {
            expect(res.statusCode).to.eql(200);
            expect(res.body).to.be.an('object');
            expect(res.body.body).to.eql(newsBody);
            done();
          })
      });
  });

  it('it should DELETE news', (done) => {
    const news = {
      title: "Название новости авдадвадвадвдв",
      body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    };
    chai.request(serverUrl)
      .post('/news')
      .send(news)
      .end((err, res) => {
        const newsId = res.body._id;
        chai.request(serverUrl)
          .delete(`/news/${newsId}`)
          .end((err, res) => {
            expect(res.statusCode).to.eql(200);
            done();
          });
      });
  });
});
